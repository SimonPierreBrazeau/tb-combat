import sys
from menu import Menu

class Entity:
    def __init__ (self, name, hp, speed, df, st, dx, mg, crDmg, crChn):
        self.name = name
        self.hp = hp
        self.speed = speed
        self.defense = df
        self.strengh = st
        self.dexterity = dx
        self.intelligence = mg
        self.criticalDamage = crDmg
        self.criticalChance = crChn


class Engine:
    def __init__ (self):
        self.player = Entity("pl", 50, 6, 7, 4, 7, 3, 25, 10)
        self.enemy = Entity("slime", 20, 2, 2, 1, 1, 1, 25, 10)

    def _printHelp (self):
        print("Here are the commands you can run:")
        print("  explore - Look around.")
        print("  help    - Lists the commands that can be ran.")
        print("  quit    - Exits the program.")
        print("")

    def _displayEncounterOptions (self):
        msg = ""
        msg += "Here are some options available to you: \n"
        msg += "  battle  - Fight the encounter.\n" 
        msg += "  run     - Run away from encounter.\n" 
        msg += "  inspect - Gather information from encounter."
        msg += "\n"

        print(msg)

    def _explore (self):
        # Create a message with the enemy's name
        encounter_message = "You found a " + self.enemy.name + ". What now?"
        print(encounter_message)
        self._displayEncounterOptions()

    def Run (self):
        print("Running engine...")

        # Main pass of menu
        menuDone = False

        while not menuDone:
            # Grab input from user
            user_input = input(">")

            user_input = str.upper(user_input)

            # Go through input
            if user_input == "EXPLORE":
                print("Exploring...")
                self._explore()
            elif user_input == "HELP":
                self._printHelp()
            elif user_input == "QUIT":
                menuDone = True
            else:
                print("Bad input! Please enter a valid command (type help for info).")

def temp (args):
    print("This is a test.")

if __name__ == "__main__":
    en = Engine()
    m = Menu("Main")
    m.add_menu_item("TEST", "A test command")
    m.Run()
    #en.Run()
