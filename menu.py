import sys
import math

class Menu:
    def __init__ (self, name):
        self.cmdNames = [
                "HELP",
                "QUIT"
                ]
        self.cmdDesc = [
                "Display list of commands",
                "Exits the menu"
                ]
        self.cmdMethods = [
                self.help,
                self.quit
                ]
        self.cmdCompleter = [
                False,
                True
                ]

        self.name = name
        self.HELP_ITEMS_PER_PAGE = 6
        self.helpPages = 0
        self.lastOptionCount = 0

    def _countItems (self):
        if len(self.cmdNames) != self.lastOptionCount:
            self.lastOptionCount = len(self.cmdNames)
            self.helpPages = math.ceil(len(self.cmdNames) / self.HELP_ITEMS_PER_PAGE)

    def Run (self):
        menuDone = False

        while not menuDone:
            uin = input("> ")
            uin = str.upper(uin)

            # Split input in words
            ucmd = str.split(uin, ' ')
            
            # Go through commands
            cid = 0
            while cid < len(self.cmdNames):
                if ucmd[0] == self.cmdNames[cid]:
                    break
                else:
                    cid += 1

            if cid < len(self.cmdNames):
                # Check if there is a command to run
                if self.cmdMethods[cid] != None:
                    # Grab arguments from input
                    args = ucmd
                    args.pop(0)
                    # Run command
                    comp = self.cmdMethods[cid](args)

                    if comp == True:
                        menuDone = self.cmdCompleter[cid]
                else:
                    # There is no command to run
                    print("Command {0} has no functionnality.".format(self.cmdNames[cid]))
            else:
                # Print error message
                print("Please enter a valid command (type help for more info).")

    def help (self, args):
        page = 0

        # Get page number from argument
        if args != None:
            if len(args) > 0:
                if args[0].isdigit():
                    page = int(args[0]) - 1
                else:
                    print("HELP: invalid argument '" + args[0] + "'.")
                    return False

        # Calculate number of pages
        self._countItems()

        # Print a list of commands
        print(self.name + " - HELP page " + str(page + 1) + "/" + str(self.helpPages))

        pc = page * self.HELP_ITEMS_PER_PAGE
        pl = pc + self.HELP_ITEMS_PER_PAGE

        while pc < pl and pc < len(self.cmdNames):
            option = "  "
            option += self.cmdNames[pc]
            option += " - "
            option += self.cmdDesc[pc]
            print(option)
            pc += 1

        return True

    def quit (self, agrs):
        return True

    def add_menu_item (self, cmd_name, cmd_desc, cmd_method = None, completer = True):
        """ Adds a command to the menu. """
        self.cmdNames.append(cmd_name)
        self.cmdDesc.append(cmd_desc)
        self.cmdMethods.append(cmd_method)
        self.cmdCompleter.append(completer)
        self.sort_commands()

    def sort_commands (self):
        """ Sorts the list of commands in alphabetical order. """
        pass
